package dz2.board;

import dz2.GameState;
import dz2.Move;
import dz2.Result;
import dz2.cell.Cell;
import dz2.cell.CellState;

public interface Position {

  Result makeMoveAndChangePosition(Move move);

  Result analysePositionAndGetResult(CellState current);

  boolean isValid(Move move);

  Cell[][] getPositionAsArray();

  Move[] getPossibleMoves(GameState state);
}
