package dz2;

import dz2.cell.CellState;

public class GameState implements Cloneable{
    private int turn;
    public GameState() {
        turn = 0;
    }
    public GameState(GameState other) {
        turn = other.turn;
    }
    public void changeTurn() {
        turn = (turn + 1) % 2;
    }
    public boolean isFirstPlayerMove() {
        return turn == 0;
    }
    public CellState toCellState() {
        return isFirstPlayerMove() ? CellState.X : CellState.O;
    }

    @Override
    public GameState clone() {
        try {
            GameState clone = (GameState) super.clone();
            clone.turn = turn;
            return clone;
        } catch (CloneNotSupportedException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof GameState) {
            return this.turn == ((GameState) obj).turn;
        }
        return false;
    }

    @Override
    public String toString() {
        return Integer.toString(turn);
    }
}
