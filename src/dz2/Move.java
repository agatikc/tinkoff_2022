package dz2;

import dz2.cell.CellState;

public class Move {
    //TODO: слишком уж сильно похоже на класс Cell
    private final int row, column;
    private final CellState value;

    public Move(int row, int column, GameState turn) {
        this.row = row;
        this.column = column;
        this.value = turn.toCellState();
    }

    public int getRow() {
        return row;
    }

    public int getColumn() {
        return column;
    }

    public CellState getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "row " + row + ", column " + column;
    }
}
