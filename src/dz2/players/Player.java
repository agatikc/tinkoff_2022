package dz2.players;

import dz2.GameState;
import dz2.Move;
import dz2.board.Position;

public abstract class Player {
    private final String name;

    public Player(String name) {
        this.name = name;
    }

    public abstract Move move(GameState turn, Position position);

    public String getName() {
        return name;
    }
}
