package dz2;

public enum Result {
  CONTINUE, AGAIN, WIN, DRAW
}
