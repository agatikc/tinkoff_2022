package dz2;

import dz2.board.TicTacToeBoard;
import dz2.players.HumanPlayer;
import dz2.players.MinimaxPlayer;
import dz2.players.Player;
import java.util.Scanner;

public class TicTacToe extends Game {

  public TicTacToe(Player player1, Player player2) {
    super(player1, player2);
  }

  private void start() {
    System.out.print("Введите размер поля: ");
    final Scanner scanner = new Scanner(System.in);
    final int boardSize = scanner.nextInt();
    //int [][] arr = {{1, -1, -1}, {1,-1,1,}, {0, 0, 0}};
    final TicTacToeBoard board = new TicTacToeBoard(boardSize);
    play(board);
    scanner.close();
  }

  public static void main(String[] args) {
    new TicTacToe(new HumanPlayer("Agatha"),
        new MinimaxPlayer("Second computer", 2)).start();
  }
}
