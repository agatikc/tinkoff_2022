package dz1;

import java.util.Arrays;
import java.util.stream.IntStream;

public class Task4 {
    private static final Object mutex = new Object();
    private static final PrinterState state = new PrinterState();

    public static void runPrinting() {
        Thread[] threads = IntStream.rangeClosed(1, 3)
                .mapToObj((id) -> new Thread(new PrintThread(id, 6)))
                .toArray(Thread[]::new);
        Arrays.stream(threads).forEach(Thread::start);
    }

    private record PrintThread(int id, int iterations) implements Runnable {
        @Override
            public void run() {
                for (int j = 0; j < iterations; j++) {
                    try {
                        synchronized (mutex) {
                            while (state.getCurrent() != id) {
                                mutex.wait();
                            }
                            System.out.print(id);
                            state.next();
                        }
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                }
            }
        }


    private static class PrinterState {
        private int current = 1;
        public void next() {
            current = 1 + current % 3;
            mutex.notifyAll();
        }
        public int getCurrent() {
            return current;
        }
    }

    public static void main(String[] args) {
        runPrinting();
    }
}
