package dz1;

import java.util.Arrays;
import java.util.List;

public class Task1 {
    public static List<Integer> square56(List<Integer> list){
        return list.stream()
                .map(e -> e * e + 10)
                .filter(e -> e % 10 != 5 && e % 10 != 6)
                .toList();
    }

    public static void main(String[] args) {
        test(3, 1, 4);
        test(1);
        test(2);
    }

    private static void test(Integer... list) {
        System.out.println(square56(Arrays.asList(list)));
    }
}